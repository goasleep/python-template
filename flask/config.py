# coding=utf-8
import configparser
import os

class ConfigUtils:
    def __init__(self):
        env = os.getenv('FLASK_ENV')
        if env == 'production':
            config_path = os.path.join(os.path.abspath('.'), "production.ini")
        else:
            config_path = os.path.join(os.path.abspath('.'), "develop.ini")

        self.cf = configparser.ConfigParser()
        self.cf.read(config_path,'utf8')  # 读取配置文件

    def get_config(self):
        return self.cf


conf = ConfigUtils()
cf = conf.get_config()

serverPort = 5000

# 接口请求日志开关
logSwitch = cf.get('Log','LogSwitch')


class Config:
    ENV = cf.get('Flask','ENV')
    DEBUG = cf.get('Flask','DEBUG')
    SQLALCHEMY_COMMIT_ON_TEARDOWN = False
    SQLALCHEMY_DATABASE_URI ="mysql+pymysql://"+cf.get('Database','username')+":"+cf.get('Database','password')+"@"+cf.get('Database','host')+":"+cf.get('Database','port')+"/limanet?charset=utf8"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    @staticmethod
    def init_app(app):
        pass